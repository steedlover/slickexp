import {
  Component,
  OnInit
} from '@angular/core';

import { Router } from '@angular/router';
import { AppState } from '../app.service';
import { User, UserServ } from '../users.service';

@Component({
  /**
   * The selector is what angular internally uses
   * for `document.querySelectorAll(selector)` in our index.html
   * where, in this case, selector is the string 'sports'.
   */
  selector: 'create-user',
  templateUrl: './create-user.component.html'
})
export class CreateUserComponent implements OnInit {

  public user: Object = {} as User;

  constructor(
    public appState: AppState,
    public userServ: UserServ,
    public router: Router
  ) {}

  public ngOnInit() {
    console.log('hello `Creaet user` component');
  }

  public createUser(vm: User) {
    this.userServ.addUser(vm);
    this.router.navigateByUrl('/sports');
  }

}
