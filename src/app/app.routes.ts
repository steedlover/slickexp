import { Routes } from '@angular/router';
import { SportsComponent } from './sports';
import { InPlayComponent } from './in-play';
import { CasinoComponent } from './casino';
import { CreateUserComponent } from './create-user';
import { LoginComponent } from './login';
import { NoContentComponent } from './no-content';

export const ROUTES: Routes = [
  { path: '',      component: SportsComponent },
  { path: 'sports',  component: SportsComponent },
  { path: 'in-play',  component: InPlayComponent },
  { path: 'casino',  component: CasinoComponent },
  { path: 'create-user',  component: CreateUserComponent },
  { path: 'login',  component: LoginComponent },
  //{ path: 'detail', loadChildren: './+detail#DetailModule'},
  //{ path: 'barrel', loadChildren: './+barrel#BarrelModule'},
  { path: '**',    component: NoContentComponent },
];
