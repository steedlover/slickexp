import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";
import "rxjs/add/operator/share";

export interface User {
  name: string;
  password: string;
}

@Injectable()
export class UserServ {

  public usrList: Array<User> = [];

  public _stat: Observable<any>;
  private observer: Observer<Boolean>;
  private usrState: Boolean = false;

  public _profile: Observable<any>;
  private pobserver: Observer<User>;
  private curUser: User = {} as User;

  constructor() {
    this.usrList.push({
      name: "admin",
      password: "admin"
    });
    this._stat = new Observable(observer => this.observer = observer).share();
    this._profile = new Observable(pobserver => this.pobserver = pobserver).share();
  }

  public addUser(input: User) {
    this.usrList.push(input);
    this.checkUser(input);
  }

  public checkUser(vm: User) {
    let result = this.usrList.filter((e) => e.name === vm.name && e.password === vm.password);
    if (result.length > 0) {
      this.usrState = true;
      this.curUser = result[0];
      this.pobserver.next(this.curUser);
      this.observer.next(this.usrState)
    }
    return result.length > 0 ? true : false;
  }

  public getCurState() {
    return this.usrState;
  }

}
