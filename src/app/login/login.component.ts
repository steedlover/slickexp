import {
  Component,
  OnInit
} from '@angular/core';

import { Router } from '@angular/router';
import { AppState } from '../app.service';
import { User, UserServ } from '../users.service';

@Component({
  /**
   * The selector is what angular internally uses
   * for `document.querySelectorAll(selector)` in our index.html
   * where, in this case, selector is the string 'sports'.
   */
  selector: 'login',
  /**
   * Every Angular template is first compiled by the browser before Angular runs it's compiler.
   */
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  /**
   * Set our default values
   */
  public lg: Object = {} as User;
  public localState = { value: '' };

  constructor(
    public appState: AppState,
    public userServ: UserServ,
    public router: Router
  ) { }

  public ngOnInit() {
  }

  public login(res: User) {
    if (this.userServ.checkUser(res)) {
      this.router.navigateByUrl('/sports');
    } else {
      alert('User name and password are incorrect!');
    }
  }

}
