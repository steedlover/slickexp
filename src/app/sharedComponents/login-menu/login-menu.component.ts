import { Component, OnInit, Inject } from '@angular/core';
import { User, UserServ } from '../../users.service';

@Component({
  selector: 'login-menu',
  templateUrl: './login-menu.component.html'
})
export class LoginMenu implements OnInit {

  private userMenu: Array<{}>;
  private msgMenu: Array<{}>;
  private moneyMenu: Array<{}>;
  private userState: Boolean;
  private curUser: User = {} as User;

  constructor(public userServ: UserServ) {}

  public ngOnInit() {
    this.userState = this.userServ.getCurState();
    this.userServ._stat.subscribe(upd => this.userState = upd);
    this.userServ._profile.subscribe(upd => this.curUser = upd);
    this.userMenu = [
      {
        title: "Change password"
      },
      {
        title: "Logout"
      }
    ];
    this.msgMenu = [
      {
        title: "You have 0 messages"
      },
      {
        title: "Create a new message"
      }
    ];
    this.moneyMenu = [
      {
        title: "Charge your account"
      },
      {
        title: "Transfer the money"
      }
    ];
    console.log('Initial LoginMenu');
  }

}
