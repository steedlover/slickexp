import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'current-time',
  template: `{{curDate | amDateFormat:'h:mm'}}&nbsp;GMT{{curDate | amDateFormat:'Z'}}&nbsp;|
  {{curDate | amDateFormat:'dddd'}},
  {{curDate | amDateFormat:'D'}}&nbsp;{{curDate | amDateFormat:'MMMM'}}`
})
export class CurrentTime implements OnInit {
  public curDate: Date;

  constructor () {
    this.curDate = new Date();
  }

  public ngOnInit() {
    console.log('Initial Current-time');
  }

}
