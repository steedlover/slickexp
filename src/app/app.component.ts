/**
 * Angular 2 decorators and services
 */
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { environment } from 'environments/environment';
import { AppState } from './app.service';

/**
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  template: `
    <div class="super-top">
      <ul>
        <li><current-time class="current-time"></current-time></li>
        <li><login-menu></login-menu></li>
        <li>
          <mat-form-field class="compact">
            <mat-select placeholder="Help">
              <mat-option *ngFor="let option of helpMenu" [value]="option.value">{{option.title}}</mat-option>
            </mat-select>
          </mat-form-field>
        </li>
        <li>
          <mat-form-field class="compact">
            <mat-select placeholder="Lang">
              <mat-option *ngFor="let option of langMenu" [value]="option.label">
                <img src="./assets/img/blank.gif" class="flag flag-{{option.class}}" />&nbsp;{{option.title}}
              </mat-option>
            </mat-select>
          </mat-form-field>
        </li>
        <li>
          <button class="btn btn-config" (click)="configClick()"><i class="fa fa-gears"></i></button>
        </li>
      </ul>
    </div>

    <div class="menu-wrapper">
      <div class="logo">
        <a [routerLink]=" ['./'] " class="logo">
          <img src="./assets/img/logo.png" alt="Logo" />
        </a>
      </div>
      <top-menu></top-menu>
      <ul class="empty-menu">
        <li></li>
      </ul>
    </div>

    <main class="content">
      <router-outlet></router-outlet>
    </main>
  `
})
export class AppComponent implements OnInit {
  /*public name = 'Angular Starter';
  public tipe = 'assets/img/tipe.png';
  public twitter = 'https://twitter.com/gdi2290';
  public url = 'https://tipe.io';*/
  public showDevModule: boolean = environment.showDevModule;
  public helpMenu: Array<Object>;
  public langMenu: Array<Object>;

  constructor(public appState: AppState) {
    this.helpMenu = [
      {
        title: "Call service",
        value: "call"
      },
      {
        title: "Account info",
        value: "info"
      }
    ];

    this.langMenu = [
      {
        label: "En",
        class: "io",
        title: "English"
      },
      {
        label: "De",
        class: "de",
        title: "Germany"
      },
      {
        label: "Mt",
        class: "mt",
        title: "Malta"
      }
    ];
  }

  public configClick() {
    alert('Config button clicked!');
  }

  public ngOnInit() {
    console.log('Initial App State', this.appState.state);
  }

}
